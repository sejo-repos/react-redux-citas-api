# React Redux Citas

Ejemplo de proyecto con react hooks, redux, abm citas con localstorage, logger, router, login y llamadas api.

[Demo](https://react-redux-citas-api.netlify.app/)

# NPM package

```
npx create-react-app my-app
cd my-app
npm start
```
```
npm i --save react-router-dom
```
```
npm i -g npm-check-updates
ncu -u #indica que librerías deberían actualizarse
npm i
npm update #para instalar nuevas versiones
```
```
npm i react-redux
npm i redux
npm i redux-thunk
```
```
npm install uuidv4
```
```
npm install axios
```
```
npm i --save redux-logger
```

# Create files and folders

```
my-app/

 README.md

 node_modules/

 package.json

 public/

  index.html

  favicon.ico

 src/
 
  reducers/
  
	dates/
	  
	index.js
  
  components/
  
	dates/
	
	App.js
  
  actions/
  
   datesActions.js

   types.js
   
  store.js

  index.js
```

# Redux store.js

    import {createStore, applyMiddleware, compose} from  'redux'
    
    import  thunk  from  'redux-thunk'
    
    //reducer principal
    
    import  rootReducer  from  './reducers'
    
    //state inicial
    
    const  initialState = {}
    
    //potenciador
    
    const  middleware = [thunk]
    
    //se crea store toma 3 parametros:
    
    //1-reducer los cambios que se hagan en el state vendran del reducer
    
    //2-state
    
    //3-potenciador
    
    const  store = createStore(
	    rootReducer, 
	    initialState, 
	    compose(applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ))
    
    export  default  store


## app.js

    //redux
    import  store  from  "../store";
    import { Provider } from  "react-redux";
    
    function App() {
	    return (
		    <Provider  store={store}>
			    <div  className="container">
			    </div>
		    </Provider>
	    );
	}
    export  default  App;

## REDUCERS  index.js

    import {combineReducers} from  'redux'
    export  default  combineReducers({})

## ACTIONS types.js

    export const SHOW_DATES = "SHOW_DATES";
    export const ADD_DATES = "ADD_DATES";
    export const DELETE_DATES = "DELETE_DATES";


## datesReducers.js

    import { SHOW_DATES, DELETE_DATES, ADD_DATES } from "../../actions/types";
    
    //estado inicial, cada reducer tiene su state
    const initialState = {
     dates: [
	     {
		     id: 0,
		     date: "10-20-30",
		     hour: "10:30",
		     pet: "Perro 1",
		     owner: "Raul",
		     symptoms: "Sintomas 1",
	     },
     ],
    };
    
    //va realizar la accion
    export default function (state = initialState, action) {
     switch (action.type) {
	     case SHOW_DATES:
	     return {
		     ...state,
	     };
	     case ADD_DATES:
	     return {
		     ...state,
		     dates: [...state.dates, action.payload],
	     };
	     case DELETE_DATES:
	     return {
		     ...state,
		     dates: state.dates.filter((item) => item.id !== action.payload),
	     };
	     default:
		     return state;
     }
    }

## datesActions.js

    import { SHOW_DATES, ADD_DATES, DELETE_DATES } from "./types";
    
    export const getDates = () => {
     return {
	     type: SHOW_DATES,
     };
    };
    export const addDate = (date) => {
     return {
	     type: ADD_DATES,
	     payload: date,
     };
    };
    export const deleteDate = (id) => {
     return {
	     type: DELETE_DATES,
	     payload: id,
     };
    };

## List data

```
import { useSelector } from "react-redux";
```
```
const datesSelector = useSelector((state) => state.dates.dates);
```
	
## Delete data

```
import {deleteDate} from '../../actions/datesActions'
import {useDispatch} from 'react-redux'
```
```
const dispath = useDispatch()
```
```
 onClick={() => dispath(deleteDate(props.date.id))}
```
	
## Add data

```
import { addDate } from "../../actions/datesActions";
import { useDispatch } from "react-redux";
```
```
const dispath = useDispatch();
```
```
 dispath(addDate(dateCopy))
```

# Logger

    import logger from 'redux-logger';
    
    const middleware = [thunk,logger]

    const store = createStore(
    rootReducer, 
    initialState, 
    compose(applyMiddleware(...middleware), 
    //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
))



# Router

    import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

## App.js

    function App() {
        return (
            <Provider store={store}>
                <Router>
                    <Header title="Name"/>
                    <div className="container">
                        <Switch>
                            <Route exact path="/" component={Landing}/>
                            <Route exact path="/login" component={Login}/>
                            <Route exact path="/dates" component={Dates}/>
                            <Route exact path="/home" component={Home}/>
                            <Route exact path="/products" component={Products}/>
                        </Switch>
                    </div>
                </Router>
            </Provider>
        );
    }

## Links

    import { NavLink } from "react-router-dom";

    <NavLink className="navbar-brand" to="/">
        {titulo}
    </NavLink>

    <li className="nav-item">
        <NavLink className="nav-link" to="/">
            Home
        </NavLink>
    </li>


## Private Route

    import { Route, Redirect } from "react-router-dom";

    export default function PrivateRoute({ children, ...rest }) {

    return (
            <Route
                {...rest}
                render={({ location }) =>
                    false ? (
                        children
                    ) : (
                        <Redirect to={{ pathname: "/login", state: { from: location } }} />
                    )
                }
            />
        );
    }


## Add private route
    import PrivateRoute from './PrivateRoute'
    
    <PrivateRoute exact path="/home"><Home /></PrivateRoute>
