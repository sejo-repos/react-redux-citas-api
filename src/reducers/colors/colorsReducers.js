import {
  FETCH_COLORS_REQUEST,
  FETCH_COLORS_SUCCESS,
  FETCH_COLORS_FAILURE,
} from "../../actions/types";

const initialState = {
  colors: {
    colorDog:'',
    colorCat:''
  },
  error: "",
  isLoding: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_COLORS_REQUEST:
      return {
        ...state,
        isLoding: true,
      };
    case FETCH_COLORS_SUCCESS:
      return {
        ...state,
        isLoding: false,
        colors: action.payload,
      };
    case FETCH_COLORS_FAILURE:
      return {
        ...state,
        isLoding: false,
        error: action.payload,
        colors: initialState.colors,
      };
    default:
      return state;
  }
}
