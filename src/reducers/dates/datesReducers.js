import { SHOW_DATES, DELETE_DATES, ADD_DATES } from "../../actions/types";

//estado inicial, cada reducer tiene su state
const initialState = {
  dates: [
    /*     {
      id: 0,
      date: "10-20-30",
      hour: "10:30",
      pet: "Perrito 1",
      owner: "Raul",
      symptoms: "Sintomas 1",
    }, */
  ],
};

//va realizar la accion
export default function (
  state = localStorage.getItem("dates")
    ? JSON.parse(localStorage.getItem("dates"))
    : initialState,
  action
) {
  switch (action.type) {
    case SHOW_DATES:
      return {
        ...state,
      };
    case ADD_DATES:
      return {
        ...state,
        dates: [...state.dates, action.payload],
      };
    case DELETE_DATES:
      return {
        ...state,
        dates: state.dates.filter((item) => item.id !== action.payload),
      };
    default:
      return state;
  }
}
