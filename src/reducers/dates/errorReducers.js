import { SHOW_ERROR_DATES } from "../../actions/types";

//estado inicial, cada reducer tiene su state
const initialState = {
  error: false,
};

//va realizar la accion
export default function (state = initialState, action) {
  switch (action.type) {
    case SHOW_ERROR_DATES:
      return {
        error: action.payload,
      };
    default:
      return state;
  }
}
