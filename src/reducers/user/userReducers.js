import {
  LOGIN_REQUEST,
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
  LOGOUT
} from "../../actions/types";

let user = JSON.parse(localStorage.getItem('user'));
const initialState = {
  user: user ? user: {},
  error: "",
  isLoding: false,
  isLoggedIn:user ? true : false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        isLoding: true,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        isLoding: false,
        user: {},
        error: action.payload,
        isLoggedIn:false
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoding: false,
        error: "",
        user: action.payload,
        isLoggedIn:true
      };
    case LOGOUT:
      return {
        ...state,
        error: "",
        user:{},
        isLoggedIn:false
      };
    default:
      return state;
  }
}
