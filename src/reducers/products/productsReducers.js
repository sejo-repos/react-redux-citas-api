import {
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  PUT_PRODUCT_REQUEST,
  PUT_PRODUCT_SUCCESS,
  PUT_PRODUCT_FAILURE,
  POST_PRODUCT_REQUEST,
  POST_PRODUCT_SUCCESS,
  POST_PRODUCT_FAILURE,
  DELETE_PRODUCT_REQUEST,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAILURE,
} from "../../actions/types";

const initialState = {
  products: [],
  error: "",
  isLoding: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_PRODUCTS_REQUEST:
      return {
        ...state,
        isLoding: true,
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        isLoding: false,
        products: action.payload,
      };
    case FETCH_PRODUCTS_FAILURE:
      return {
        ...state,
        isLoding: false,
        error: action.payload,
        products: [],
      };
    case PUT_PRODUCT_REQUEST:
      return {
        ...state,
        isLoding: true,
        error: "",
      };
    case PUT_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoding: false,
        products: state.products.map((product) =>
          product.id === action.payload.id
            ? (product = action.payload)
            : product
        ),
        error: "",
      };
    case PUT_PRODUCT_FAILURE:
      return {
        ...state,
        isLoding: false,
        error: action.payload,
        products: state.products,
      };
    case POST_PRODUCT_REQUEST:
      return {
        ...state,
        isLoding: true,
      };
    case POST_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoding: false,
        products: [...state.products, action.payload],
      };
    case POST_PRODUCT_FAILURE:
      return {
        ...state,
        isLoding: false,
        error: action.payload,
        products: state.products,
      };
    case DELETE_PRODUCT_REQUEST:
      return {
        ...state,
        isLoding: true,
      };
    case DELETE_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoding: false,
        products: state.products.filter(
          (product) => product.id !== action.payload
        ),
      };
    case DELETE_PRODUCT_FAILURE:
      return {
        ...state,
        isLoding: false,
        error: action.payload,
        products: state.products,
      };
    default:
      return state;
  }
}
