import { combineReducers } from "redux";
import datesReducers from "../reducers/dates/datesReducers";
import errorReducers from "../reducers/dates/errorReducers";
import colorsReducers from "../reducers/colors/colorsReducers";
import productsReducers from "../reducers/products/productsReducers";
import userReducers from "../reducers/user/userReducers";

export default combineReducers({
  dates: datesReducers,
  errorDate: errorReducers,
  colors:colorsReducers,
  products:productsReducers,
  user:userReducers
});
