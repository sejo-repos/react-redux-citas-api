import React from "react";
import { HashRouter as Router } from "react-router-dom";

//redux
import { useDispatch } from "react-redux";
import { logout } from "../actions/user/userActions";

//components
import Routes from "./routes";
import Header from "./header";

//redux
import { useSelector } from "react-redux";

//helper
import { history } from "./helpers/Helpers";

function App(props) {
  const dispath = useDispatch();
  const isLoggedIn = useSelector((state) => state.user.isLoggedIn);

  const handleLogout = () => {
    dispath(logout(history));
  };

  return (
    <Router>
      <Header
        title="Name"
        handleLogout={handleLogout}
        isLoggedIn={isLoggedIn}
      />
      <div className="container">
        <Routes />
      </div>
    </Router>
  );
}

export default App;
