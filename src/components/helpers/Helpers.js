import React from "react";
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();

export const AlertError = (error) => {
  return (
    <div
      className="alert alert-danger alert-dismissible fade show"
      role="alert"
    >
      <strong>Error!</strong> {error}
      <button
        type="button"
        className="close"
        data-dismiss="alert"
        aria-label="Close"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  );
};

export const Spinner = () => {
  return (
    <div className="spinner-border text-primary" role="status">
      <span className="sr-only">Loading...</span>
    </div>
  );
};

export const Toast = (msj) => {
  return (
    <div
      aria-live="polite"
      aria-atomic="true"
      style={{ position: "relative", minHeight: "200px" }}
    >
      <div className="toast" style={{ position: "absolute", top: 0,right: 0, }}>
        <div className="toast-header">
          <strong className="mr-auto">Bootstrap</strong>
          <small>11 mins ago</small>
          <button
            type="button"
            className="ml-2 mb-1 close"
            data-dismiss="toast"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="toast-body">{msj}</div>
      </div>
    </div>
  );
};
