import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

//https://getbootstrap.com/docs/4.5/examples/offcanvas/

function Header(props) {
  const { handleLogout, isLoggedIn } = props;
  const [navBackground, setNavBackground] = useState(
    "navbar navbar-expand-lg fixed-top navbar-light bg-transparent"
  );
  const [navbarCollapse, setNavbarCollapse] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 30;
      if (!show) {
        setNavBackground(
          "navbar navbar-expand-lg fixed-top navbar-light bg-transparent"
        );
      } else {
        setNavBackground(
          "navbar navbar-expand-lg fixed-top navbar-light shadow-sm mb-5 bg-white"
        );
      }
    };
    document.addEventListener("scroll", handleScroll);
    return () => {
      document.removeEventListener("scroll", handleScroll);
    };
  }, []);

  function handleOffcanvas() {
    setNavbarCollapse(!navbarCollapse);
  }

  const linkPrivates = () => {
    if (isLoggedIn) {
      return (
        <>
          <li className="nav-item">
            <NavLink className="nav-link" to="/home" onClick={handleOffcanvas}>
              Home
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              className="nav-link"
              to="/products"
              onClick={handleOffcanvas}
            >
              Productos
            </NavLink>
          </li>
        </>
      );
    }
  };

  return (
    <nav className={navBackground}>
      <button
        className="navbar-toggler p-0 border-0"
        type="button"
        data-toggle="offcanvas"
        onClick={handleOffcanvas}
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="d-lg-none p-1"></div>
      <NavLink
        className="navbar-brand mr-auto mr-lg-0"
        to="/"
        onClick={handleOffcanvas}
      >
        <strong>Lorem</strong>Ipsum
      </NavLink>
      <div
        className={
          navbarCollapse
            ? "navbar-collapse offcanvas-collapse open"
            : "navbar-collapse offcanvas-collapse"
        }
        id="navbarsExampleDefault"
      >
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <NavLink className="nav-link" to="/dates" onClick={handleOffcanvas}>
              Citas
            </NavLink>
          </li>
          {linkPrivates()}
        </ul>
        {isLoggedIn ? (
          <button
            className="btn btn-outline-danger my-2 my-sm-0 mr-2"
            onClick={handleLogout}
          >
            Salir
          </button>
        ) : (
          <NavLink
            className="btn btn-outline-secondary my-2 my-sm-0"
            to="/login"
            onClick={handleOffcanvas}
          >
            Iniciar sesión
          </NavLink>
        )}
      </div>
    </nav>
  );
}

export default Header;
