import React from "react";
import { Route, Switch } from "react-router-dom";

//import { useSelector } from "react-redux";

//components
import Dates from "../dates";
import Landing from "../landing";
import Login from "../login";
import Home from "../home";
import Products from "../products";

import PrivateRoute from "./PrivateRoute";

function Routes(props) {
    //const isLoggedIn = useSelector((state) => state.user.isLoggedIn);

  return (
    <Switch>
      <Route exact path="/" component={Landing} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/dates" component={Dates} />
      <PrivateRoute exact path="/home">
        <Home />
      </PrivateRoute>
      <PrivateRoute exact path="/products">
        <Products />
      </PrivateRoute>
    </Switch>
  );
}

export default Routes;
