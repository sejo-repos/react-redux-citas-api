import React from "react";

const Home = (props) => {
  return (
    <div className="alert bg-customColor mt-2 shadow-sm p-3 mb-5 rounded" role="alert">
      <h4 className="alert-heading text-uppercase text-white"><i className="fas fa-paw mr-1"></i>bienvenido!</h4>
      <p>
        Aww yeah, you successfully read this important alert message. This
        example text is going to run a bit longer so that you can see how
        spacing within an alert works with this kind of content.
      </p>
      <hr />
      <p className="mb-0 font-italic">
        Whenever you need to, be sure to use margin utilities to keep things
        nice and tidy.
      </p>
    </div>
  );
};

export default Home;
