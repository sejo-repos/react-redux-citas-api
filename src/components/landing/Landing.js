import React from "react";

//components
import CustomButton from "../button/CustomButton";
import MainSvg from "./MainSvg";

//styles
import styles from "./style.module.css";

const Landing = () => {

  return (
    <>
      <div className={"row " + styles.landingMarginTop}>
        <div className={"col-xs-12 col-lg-6 " + styles.landingCol}>
          <div>
            <p className={styles.landingTitle}>
              Lorem ipsum dolor <br />
              sit amet?
            </p>
            <p className={"font-weight-light " + styles.landingText}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus
              at tempus ipsum. Duis nec eros at magna dignissim volutpat sit
              amet et sapien. Donec lobortis, neque vitae maximus.
            </p>
            <CustomButton linkTo="/dates">Reservar</CustomButton>
          </div>
        </div>
        <div className="col-xs-12 col-lg-6">
            <MainSvg />
        </div>
      </div>
    </>
  );
};

export default Landing;
