import React from "react";

import { deleteDate } from "../../actions/dates/datesActions";
import { useDispatch } from "react-redux";

function Date(props) {
  const dispath = useDispatch();
  return (
    <>
      <div className="media mt-3">
        <h5 className="mt-0">{props.date.pet}</h5>
      </div>
      <hr/>
      <p className="card-text">
        <strong>Nombre Dueño:</strong> {props.date.owner}
      </p>
      <p className="card-text">
        <strong><i className="fa fa-calendar"></i></strong> {props.date.date},
        <strong className="ml-1"><i className="fa fa-clock" aria-hidden="true"></i></strong> {props.date.hour}
      </p>
      <p className="card-text">
      </p>
      <p className="card-text">
        <strong>Sintomas:</strong> <br />
        {props.date.symptoms}
      </p>
      <button
        onClick={() => dispath(deleteDate(props.date.id))}
        className="btn btn-sm btn-danger"
      >
        <i className="fas fa-trash mr-1"></i>Borrar
      </button>
    </>
  );
}

export default Date;
