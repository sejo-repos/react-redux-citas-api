import React from "react";

//components
import AddDate from "./AddDate";
import ListDates from "./ListDates";

function Dates() {
  return (
    <>
      <div className="row">
        <div className="col-md-6">
          <AddDate />
        </div>
        <div className="col-md-6">
          <ListDates />
        </div>
      </div>
    </>
  );
}

export default Dates;
