import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";

//redux
import { useSelector } from "react-redux";
import { addDate } from "../../actions/dates/datesActions";
import { showError } from "../../actions/errorActions";
import { useDispatch } from "react-redux";

const initDate = {
  id: 0,
  pet: "",
  owner: "",
  date: "",
  hour: "",
  symptoms: "",
};

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

function AddDate() {
  const [date, setDates] = useState(initDate);
  const [isLoding, setIsLoading] = useState(false);
  const errorSelector = useSelector((state) => state.errorDate.error);
  const dispath = useDispatch();

  React.useEffect(() => {
    
  }, [date]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const error = validateFields();
    if (!error) {
      setIsLoading(true);
      const dateCopy = {
        id: uuidv4(),
        pet: date.pet,
        owner: date.owner,
        date: date.date,
        hour: date.hour,
        symptoms: date.symptoms,
      };
      dispath(addDate(dateCopy));
      e.currentTarget.reset();
      setDates(initDate);
      await sleep(800);
      setIsLoading(false);
    }
  };

  const handleChange = (e) => {
    setDates({
      ...date,
      [e.target.name]: e.target.value,
    });
  };

  function validateFields() {
    if (date.pet !== "" && date.owner !== "" && date.symptoms !== "") {
      dispath(showError(false));
      return false;
    } else {
      dispath(showError(true));
      removeAlert();
      return true;
    }
  }

  const alertError = () => {
    if (errorSelector) {
      return (
        <div
          className="alert alert-danger alert-dismissible fade show"
          role="alert"
        >
          <strong>Error!</strong> los campos nombres y sintomas son
          obligatorios.
          <button
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
            onClick={() => dispath(showError(false))}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      );
    } else {
      return null;
    }
  };

  function removeAlert() {
    // remove alert after faded out
    setTimeout(() => {
      dispath(showError(false));
    }, 1600);
  }

  return (
    <>
      <div className="card mt-2">
        <div className="card-body">
          <h4 className="card-title text-center mb-4">Nueva cita</h4>
          {alertError()}
          <form onSubmit={handleSubmit}>
            <div className="form-group row">
              <div className="col-xs-12 col-md-12 col-lg-12">
                <input
                  type="text"
                  name="pet"
                  className="form-control"
                  placeholder="Nombre Mascota"
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-xs-12 col-md-12 col-lg-12">
                <input
                  type="text"
                  name="owner"
                  className="form-control"
                  placeholder="Nombre Dueño de la Mascota"
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-xs-12 col-md-12 col-lg-12">
                <input
                  type="date"
                  name="date"
                  className="form-control"
                  placeholder="Fecha"
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-xs-12 col-md-12 col-lg-12">
                <input
                  type="time"
                  name="hour"
                  className="form-control"
                  placeholder="Hora"
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-xs-12 col-md-12 col-lg-12">
                <textarea
                  name="symptoms"
                  className="form-control"
                  placeholder="Sintomas de la mascota..."
                  onChange={handleChange}
                ></textarea>
              </div>
            </div>
            <div className="form-group row justify-content-end mt-4">
              <div className="col-sm-4 col-md-5">
                {isLoding ? (
                  <button
                    className="btn btn-success w-100"
                    type="button"
                    disabled
                  >
                    <span
                      className="spinner-border spinner-border-sm"
                      //role="status"
                    ></span>{" "}
                    Cargando...
                  </button>
                ) : (
                  <button type="submit" className="btn btn-success w-100">
                    Agregar
                  </button>
                )}
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default AddDate;