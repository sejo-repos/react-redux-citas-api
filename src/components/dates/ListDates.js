import React from "react";
import { useSelector } from "react-redux";
import store from "./../../store";

//components
import Date from "./Date";

store.subscribe(() => {
  localStorage.setItem("dates", JSON.stringify(store.getState().dates));
});

function ListDates() {
  const datesSelector = useSelector((state) => state.dates.dates);
  
  const message =
    Object.keys(datesSelector).length === 0
      ? "No hay citas"
      : "Administra las citas aquí";

  return (
    <>
      <div>
        <div className="card mt-2 mb-3">
          <div className="card-body">
            <h4 className="card-title text-center">{message}</h4>
            <div className="lista-citas">
              {datesSelector.map((date, index) => (
                <Date key={index} date={date} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ListDates;