import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { toast } from "react-toastify";

//redux
import { useDispatch } from "react-redux";
import { postProduct } from "../../actions/products/productsActions";

const initValues = {
  name: "",
  description: "",
};

function AddProduct(props) {
  const { handleShowCreate } = props;
  const [product, setProduct] = useState(initValues);
  const dispath = useDispatch();

  const handleChange = (e) => {
    setProduct({
      ...product,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const resultValidate = validate();
    if (resultValidate) {
      const productCopy = { id: uuidv4(), ...product };
      dispath(postProduct(productCopy));
      setProduct(initValues);
      handleShowCreate();
    } else {
      toast.error("🙊 Los campos del producto son requeridos!", {
        position: "top-right",
        autoClose: 9000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  const validate = () => {
    if (product.name.length > 0 && product.description.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit} className="pt-2 pb-5">
        <div className="row">
          <div className="col">
            <input
              type="text"
              name="name"
              placeholder="Nombre"
              onChange={handleChange}
              className="form-control"
            />
          </div>
          <div className="col">
            <input
              type="text"
              name="description"
              placeholder="description"
              onChange={handleChange}
              className="form-control"
            />
          </div>
          <div className="col">
            <button
              data-toggle="tooltip"
              data-placement="top"
              title="Crear"
              type="sumit"
              className="btn btn-primary"
            >
              Crear
            </button>
          </div>
        </div>
      </form>
    </>
  );
}

export default AddProduct;
