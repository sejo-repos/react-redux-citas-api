import React, { useState, useEffect } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//redux
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { fetchProducts } from "../../actions/products/productsActions";

//components
import Product from "./Product";
import AddProduct from "./AddProduct";
import { AlertError, Spinner } from "../helpers/Helpers";

import styles from "./style.module.css";

function Products() {
  const dispath = useDispatch();
  const productsSelector = useSelector((state) => state.products);
  const products = productsSelector.products;
  const error = productsSelector.error;
  const isLoding = productsSelector.isLoding;
  const [showCreateProduct, setShowCreateProduct] = useState(false);

  useEffect(() => {
    dispath(fetchProducts());
  }, [dispath]);

  const noProducts = () => {
    return (
      <tr>
        <td colSpan={4} className="text-center">
          No hay productos
        </td>
      </tr>
    );
  };

  const handleShowCreate = () => {
    setShowCreateProduct(!showCreateProduct);
  };

  return (
    <div className="row mt-2">
      <div className="col">
        <div className="card">
          <div className="card-body">
            {isLoding ? (
              <div className="text-center p-5">
                <Spinner />
              </div>
            ) : (
              <>
                <h5 className={"card-title " + styles.alignTitle}>
                  Productos
                  <button
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Nuevo"
                    className="btn btn-outline-primary"
                    onClick={handleShowCreate}
                  >
                    +
                  </button>
                </h5>
                {!!error ? AlertError(error) : null}
                {showCreateProduct ? (
                  <AddProduct handleShowCreate={handleShowCreate} />
                ) : null}
                <div className="table-responsive">
                  <table className="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      {Object.keys(products).length > 0
                        ? products.map((item, index) => {
                            const customIndex = index + 1;
                            return (
                              <Product
                                product={item}
                                index={customIndex}
                                key={item.id}
                              />
                            );
                          })
                        : noProducts()}
                    </tbody>
                  </table>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
}

export default Products;
