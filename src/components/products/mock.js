import { v4 as uuidv4 } from "uuid";

export default {
    data: [
        {
            id: uuidv4(),
            firstName: "Cortez",
            lastName: "Mitchell"
        },
        {
            id: uuidv4(),
            firstName: "Alexandra",
            lastName: "Harris"
        },
        {
            id: uuidv4(),
            firstName: "Veronica",
            lastName: "Schoen"
        },
    ]
}