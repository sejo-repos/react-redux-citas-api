import React, { useState } from "react";
import { toast } from "react-toastify";

//styles
import styles from "./style.module.css";

//redux
import { useDispatch } from "react-redux";
import {
  putProducts,
  deleteProduct,
} from "../../actions/products/productsActions";

const Product = (props) => {
  const { product, index } = props;
  const [show, setShow] = useState(false);
  const [productToEdit, setProductToEdit] = useState(product);
  const [isDisabled, setIsDisabled] = useState(false);
  const dispath = useDispatch();

  function handleEdit() {
    setShow(!show);
  }

  function handleDelete() {
    dispath(deleteProduct(product.id));
  }

  function handleEidt() {
    setIsDisabled(true)
    const isValidate = validate();
    if(isValidate){
      dispath(putProducts(productToEdit));
      handleEdit()
    }else{
      toast.error("🙊 Los campos del producto son requeridos!", {
        position: "top-right",
        autoClose: 9000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
    setIsDisabled(false)
  }

  function handleChange(e) {
    setProductToEdit({
      ...productToEdit,
      [e.target.name]: e.target.value,
    });
  }

  function validate() {
    if (productToEdit.name === "" || productToEdit.description === "") {
      return false;
    } else {
      return true;
    }
  }

  return (
    <>
      <tr>
        <th scope="row">{index}</th>
        <td>
          {show ? (
            <input
              type="text"
              name="name"
              value={productToEdit.name}
              onChange={handleChange}
              className={"form-control " + styles.customInput}
              disabled={isDisabled}
            />
          ) : (
            productToEdit.name
          )}
        </td>
        <td>
          {show ? (
            <input
              type="text"
              name="description"
              value={productToEdit.description}
              onChange={handleChange}
              className={"form-control " + styles.customInput}
              disabled={isDisabled}
            />
          ) : (
            productToEdit.description
          )}
        </td>
        <td>
          <div className="btn-group" role="group" aria-label="Basic example">
            {show ? (
              <>
                <button
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Editar"
                  className="btn btn-info btn-sm"
                  onClick={handleEidt}
                >
                  <i className="fa fa-check"></i>
                </button>
                <button
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Eliminar"
                  className="btn btn-danger btn-sm"
                  onClick={()=>setShow(false)}
                >
                  <i className="fa fa-times"></i>
                </button>
              </>
            ) : (
              <>
                <button
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Editar"
                  className="btn btn-info btn-sm"
                  onClick={handleEdit}
                >
                  <i className="fa fa-edit"></i>
                </button>
                <button
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Eliminar"
                  className="btn btn-danger btn-sm"
                  onClick={handleDelete}
                >
                  <i className="fa fa-trash"></i>
                </button>
              </>
            )}
          </div>
        </td>
      </tr>
    </>
  );
};

export default Product;
