import React from "react";
import { NavLink } from "react-router-dom";

const style = {
  backgroundImage:
    "linear-gradient(to right, #9faedf 0%, #9cc4e5 50%, #9ae0eb 100%)",
  backgroundRepeat: "repeat-x",
  color: "#fff",
  bordercolor: "#007bff",
  display: "inline-block",
  textDecoration: "none",
  fontWeight: "400",
  textAlign: "center",
  verticalAlign: "middle",
  userSelect: "none",
  border: "1px solid",
  padding: ".375rem .75rem",
  fontSize: "1rem",
  lineHeight: "1.5",
  borderRadius: ".25rem",
  transition:
    "color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out",
};

const CustomButton = (props) => {
  const { linkTo, customClass, disabled } = props;

  if (!!linkTo) {
    return (
      <NavLink
        className={"btn " + customClass ? customClass : null}
        to={linkTo}
        style={style}
      >
        <span className="font-weight-bold">{props.children}</span>
      </NavLink>
    );
  }

  return disabled ? (
    <button disabled={disabled} className="btn btn-block btn-light">
      <span className="font-weight-bold">Cargando...</span>
    </button>
  ) : (
    <button style={style} className={"btn " + customClass ? customClass : null}>
      <span className="font-weight-bold">{props.children}</span>
    </button>
  );
};

export default CustomButton;
