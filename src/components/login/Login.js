import React, { useState } from "react";
import wave from "../../images/wave.svg";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//components
import CustomButton from "../button/CustomButton";

//redux
import { useDispatch } from "react-redux";
import { login as loginIn } from "../../actions/user/userActions";
import { useSelector } from "react-redux";

//styles
import styles from "./style.module.css";
const styleImageBackground = {
  backgroundImage: `url(${wave})`,
  backgroundSize: "cover",
  backgroundPosition: "center center",
  backgroundRepeat: "no-repeat",
};

const initialStateUser = { userName: "", password: "" };

const Login = (props) => {
  const { history } = props;
  const dispath = useDispatch();
  const [user, setUser] = useState(initialStateUser);
  const isLoading = useSelector((state) => state.user.isLoding);

  React.useEffect(() => {
    async function checkLogin() {
      const user = window.localStorage.getItem("user");
      if (user) {
        history.push("/home");
      }
    }
    checkLogin();
  }, [history]);

  const onSubmit = (e) => {
    e.preventDefault();
    dispath(loginIn(user, history));
  };

  return (
    <>
      <div className="row mt-2 justify-content-center">
        <div
          style={styleImageBackground}
          className={"shadow-sm rounded " + styles.fakeCard}
        >
          <div className={"col " + styles.fakeCardBody}>
            <h4 className={styles.loginTitle}>Iniciar sesión</h4>
            <div className={styles.formLogin}>
              <form onSubmit={onSubmit}>
                <div className="form-group my-4">
                  <input
                    type="text"
                    placeholder="Usuario: admin"
                    className="form-control"
                    name="userName"
                    onChange={(event) =>
                      setUser({
                        ...user,
                        [event.target.name]: event.target.value,
                      })
                    }
                  />
                </div>
                <div className="form-group my-4">
                  <input
                    type="password"
                    placeholder="Password: 123"
                    className="form-control"
                    name="password"
                    onChange={(event) =>
                      setUser({
                        ...user,
                        [event.target.name]: event.target.value,
                      })
                    }
                  />
                </div>
                <div className="form-group my-4">
                  <CustomButton
                    customClass="btn-block"
                    type="onSubmit"
                    disabled={isLoading}
                  >
                    Iniciar sesión
                  </CustomButton>
                </div>
              </form>
            </div>
            <div
              className={"d-flex justify-content-center " + styles.loginSocial}
            >
              <button
                type="button"
                className={"btn border " + styles.btnCircle}
                data-toggle="tooltip"
                data-placement="top"
                title="twitter"
              >
                <i className="fab fa-twitter"></i>
              </button>
              <button
                type="button"
                className={"btn border ml-1 mr-1 " + styles.btnCircle}
                data-toggle="tooltip"
                data-placement="top"
                title="instagram"
              >
                <i className="fab fa-instagram"></i>
              </button>
              <button
                type="button"
                className={"btn border " + styles.btnCircle}
                data-toggle="tooltip"
                data-placement="top"
                title="facebook"
              >
                <i className="fab fa-facebook"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer />
    </>
  );
};

export default Login;
