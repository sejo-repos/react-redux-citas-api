import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

//reducer principal
import rootReducer from "./reducers";

//state inicial
const initialState = {};

//potenciador
const middleware = [thunk, logger];

//se crea store toma 3 parametros:
//1-reducer los cambios que se hagan en el state vendran del reducer
//2-state
//3-potenciador
const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(...middleware)
    //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

export default store;