const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

// A fake authenticator to mock async api call
export async function apiLogin(user) {
  await delay(2000);
  if (user.userName === "admin" && user.password === "123"){
    return {userName: "admin"}
  }else{
    return {error: "Email o contraseña incorrecto"}
  }
}
