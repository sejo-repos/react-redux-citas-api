import axios from "axios";
import { toast } from "react-toastify";
import {
  FETCH_PRODUCTS_FAILURE,
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  PUT_PRODUCT_REQUEST,
  PUT_PRODUCT_SUCCESS,
  PUT_PRODUCT_FAILURE,
  POST_PRODUCT_REQUEST,
  POST_PRODUCT_SUCCESS,
  POST_PRODUCT_FAILURE,
  DELETE_PRODUCT_REQUEST,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAILURE,
} from "../types";

export const fetchProductsRequest = () => {
  return {
    type: FETCH_PRODUCTS_REQUEST,
  };
};

export const fetchProductsSuccess = (products) => {
  return {
    type: FETCH_PRODUCTS_SUCCESS,
    payload: products,
  };
};

export const fetchProductsFailure = (error) => {
  return {
    type: FETCH_PRODUCTS_FAILURE,
    payload: error,
  };
};

export const putProductsRequest = () => {
  return {
    type: PUT_PRODUCT_REQUEST,
  };
};

export const putProductsSuccess = (products) => {
  return {
    type: PUT_PRODUCT_SUCCESS,
    payload: products,
  };
};

export const putProductsFailure = (error) => {
  return {
    type: PUT_PRODUCT_FAILURE,
    payload: error,
  };
};

export const deleteProductRequest = () => {
  return {
    type: DELETE_PRODUCT_REQUEST,
  };
};

export const deleteProductSuccess = (id) => {
  return {
    type: DELETE_PRODUCT_SUCCESS,
    payload: id,
  };
};

export const deleteProductFailure = (error) => {
  return {
    type: DELETE_PRODUCT_FAILURE,
    payload: error,
  };
};

export const postProductsRequest = () => {
  return {
    type: POST_PRODUCT_REQUEST,
  };
};

export const postProductsSuccess = (product) => {
  return {
    type: POST_PRODUCT_SUCCESS,
    payload: product,
  };
};

export const postProductsFailure = (error) => {
  return {
    type: POST_PRODUCT_FAILURE,
    payload: error,
  };
};

export const fetchProducts = () => {
  return (dispath) => {
    dispath(fetchProductsRequest());
    axios
      .get(
        `https://my-json-server.typicode.com/sergio-balanda/json-server/products`
      )
      .then(function (response) {
        dispath(fetchProductsSuccess(response.data));
      })
      .catch(function (error) {
        const errorMsg = error.message;
        dispath(fetchProductsFailure(errorMsg));
        toast.error("🙊 Error al obtener productos!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  };
};

export const putProducts = (product) => {
  return (dispath) => {
    dispath(putProductsRequest());
    axios
      .put(
        `https://my-json-server.typicode.com/sergio-balanda/json-server/products/${product.id}`,
        product
      )
      .then(function (response) {
        dispath(putProductsSuccess(response.data));
        toast.success("🐶🐱 Producto actualizado con exito!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
      .catch(function (error) {
        const errorMsg = error.message;
        dispath(putProductsFailure(errorMsg));
        toast.error("🙊 Error al actualizar producto!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  };
};

export const postProduct = (product) => {
  return (dispath) => {
    dispath(postProductsRequest());
    axios
      .post(
        `https://my-json-server.typicode.com/sergio-balanda/json-server/products`,
        product
      )
      .then(function (response) {
        dispath(postProductsSuccess(response.data));
        toast.success("🐶🐱 Producto creado con exito!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
      .catch(function (error) {
        const errorMsg = error.message;
        dispath(postProductsFailure(errorMsg));
        toast.error("🙊 Error al crear producto!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  };
};

export const deleteProduct = (id) => {
  return (dispath) => {
    dispath(deleteProductRequest());
    axios
      .delete(
        `https://my-json-server.typicode.com/sergio-balanda/json-server/products/${id}`
      )
      .then(function (response) {
        dispath(deleteProductSuccess(id));
        toast.success("🐶🐱 Producto eliminado con exito!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
      .catch(function (error) {
        const errorMsg = error.message;
        dispath(putProductsFailure(errorMsg));
        toast.error("🙊 Error al eliminar producto!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  };
};
