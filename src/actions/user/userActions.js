import { toast } from "react-toastify";
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT
} from "../types";
import { apiLogin } from '../../api/fakeAuth'

export const userLoginRequest = () => {
  return {
    type: LOGIN_REQUEST,
  };
};

export const userLoginSuccess = (user) => {
  return {
    type: LOGIN_SUCCESS,
    payload: user,
  };
};

export const userLoginFailure = (error) => {
  return {
    type: LOGIN_FAILURE,
    payload: error,
  };
};

export const userLoguot = () => {
  return {
    type: LOGOUT,
  };
};


export const login = (user,history) => {
  return (dispath) => {
    dispath(userLoginRequest());
    apiLogin(user).then(function (response) {
      if(response.userName){
        dispath(userLoginSuccess(response));
        window.localStorage.setItem("user",JSON.stringify(response))
        history.push("/home")
      }else{
        dispath(userLoginFailure(response.error));
        toast.error(`🙊 ${response.error}`, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    }).catch(function (error) {
      console.error(error);
    })
  };
};

export const logout = (history) => {
  return (dispath) => {
    window.localStorage.removeItem('user');
    dispath(userLoguot());
    history.go(0)
  };
};


