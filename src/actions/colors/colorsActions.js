import axios from 'axios'
import {
  FETCH_COLORS_REQUEST,
  FETCH_COLORS_SUCCESS,
  FETCH_COLORS_FAILURE,
} from "../types";

export const fetchColorsRequest = () => {
  return {
    type: FETCH_COLORS_REQUEST,
  };
};

export const fetchColorsSuccess = (color) => {
  return {
    type: FETCH_COLORS_SUCCESS,
    payload: color,
  };
};

export const fetchColorsFailure = (error) => {
  return {
    type: FETCH_COLORS_FAILURE,
    payload: error,
  };
};

export const fetchColors = (customObj) => {
  return (dispath) => {
    dispath(fetchColorsRequest());
    axios 
      .get(`http://www.colr.org/json/color/random?2&timestamp=${new Date().getTime()}`)
      .then(function (response) {
        const defaulColor = "#9ae0eb";
        const color = response.data.new_color
          ? "#" + response.data.new_color
          : defaulColor;
          if(customObj.animal==='dog'){
            const copyObj = {
              colorDog: color,
              colorCat: customObj.colorCat
            }
            dispath(fetchColorsSuccess(copyObj));
          }else{
            const copyObj = {
              colorDog: customObj.colorDog,
              colorCat: color,
            }
            dispath(fetchColorsSuccess(copyObj));
          }
      })
      .catch(function (error) {
        const errorMsg = error.message;
        dispath(fetchColorsFailure(errorMsg));
      });
  };
};
