import { SHOW_DATES, ADD_DATES, DELETE_DATES } from "../types";

export const getDates = () => {
  return {
    type: SHOW_DATES,
  };
};

export const addDate = (date) => {
  return {
    type: ADD_DATES,
    payload: date,
  };
};

export const deleteDate = (id) => {
  return {
    type: DELETE_DATES,
    payload: id,
  };
};
