import { SHOW_ERROR_DATES } from "./types";

export const showError = (error) => {
  return {
    type: SHOW_ERROR_DATES,
    payload: error,
  };
};
